#!/usr/bin/env python
import os
import re
import subprocess

##Usage: module load esslurm; resubmit_via_esslurm.py

Ga_dir=re.compile("Ga\d+")
mem_line=re.compile("\#SBATCH\s+--mem")
current_dir=os.getcwd()
file_list=os.listdir(current_dir)

for dir_name in file_list:
    if Ga_dir.match(dir_name) and os.path.isdir(dir_name):

        tab_file=dir_name+"/rnaseq_gea.tab.txt"
        if os.path.exists(tab_file):
            continue

        sh_file=dir_name+"/"+dir_name+"_gea.sh"
        content=""
        with open(sh_file) as input:
            for line in input:
                if mem_line.search(line):
                    line="#SBATCH --mem=360G"+"\n"
                content+=line 
        sh_out=open(sh_file,'w+t')
        sh_out.write(content)
        sh_out.close()
        cmd="sbatch -C skylake -q jgi_shared "+sh_file
        cmd_list=cmd.split() 
        sp=subprocess.Popen(cmd_list)        
        sp.wait()


