#!/usr/bin/env python
import os
import re

##Usage: check_cori_runs.py

Ga_dir=re.compile("Ga\d+")
mem_line=re.compile("\#SBATCH\s+--mem")
current_dir=os.getcwd()
file_list=os.listdir(current_dir)

message=""

for dir_name in file_list:
    if Ga_dir.match(dir_name) and os.path.isdir(dir_name):

        tab_file=dir_name+"/rnaseq_gea.tab"
        if os.path.exists(tab_file):
            continue
        else:
            message+=dir_name+" Failed: no rnaseq_gea.tab.txt generated"+"\n"
           
if len(message)>=1:
    message+="\nresubmit the above to ExVivo:\nmodule load esslurm\n/global/homes/d/dywu/dwu_scripts/metatranscriptome_dir/gea_cori/resubmit_via_esslurm.py";
    print (message)
else:
    print ("All Cori runs are completed")

