#! /usr/bin/env python
import sys, os, argparse
import rnaseq_jamo, rnaseq_gea, startlog
import db_connect

def jrun(check_keysfn,skip_keysfn):
 logging = startlog.run('analysis')
 metadata = rnaseq_jamo.run(logging,check_keysfn,skip_keysfn)

 for jat_key in metadata:
  gap = metadata[jat_key]['gold_ap']
  reftaxon = metadata[jat_key]['ref_taxonid']
  bamfn = metadata[jat_key]['bam_floc']
  maptype = metadata[jat_key]['map_type']
  failure = validate(gap,reftaxon,bamfn)
  
  if failure != '': logging.info("Failed validation of " + str(jat_key) + " : " + failure)
  else: 
   #print "process"
   rnaseq_gea.run(gap,reftaxon,bamfn,maptype,logging,jat_key)
   #sys.exit(1)

def irun(gap,reftaxon,bamfn):
 logging = startlog.run('analysis')
 failure = validate(gap,reftaxon,bamfn)
 if failure != '': logging.info("Failed validation of " + str(jat_key) + " : " + failure)
 else:
  print "process"
  #rnaseq_gea.run(gap,reftaxon,bamfn)

def validate(gap,reftaxon,bamfn):
 fail = ''

 if os.path.isfile(bamfn): pass
 else : fail += 'Specified BAM file does not exist.'

 #duplicate release since an extry for this gold AP exists in the database
 dup_query = 'select submission_id from submission where analysis_project_id=\'' + str(gap) + '\''
 dup_subid = db_connect.imgsub_query(dup_query)
 if dup_subid : fail += 'Submission id '+ str(dup_subid) + ' in database found for gold AP ' + str(gap)

 #ADD gold AP check (for non jamo submissions)
 return(fail)


if __name__ == '__main__':
 parser = argparse.ArgumentParser(description="Metatranscriptome alignment submission to IMG")
 parser.add_argument("-j", action='store_true', help="flag for jamo pickup")
 parser.add_argument("--c", help = "file with jat keys to process (Optional)")
 parser.add_argument("--s", help = "file with jat keys to skip (Optional)")
 parser.add_argument("--gap", help="gold ap")
 parser.add_argument("--ref", help="ref img taxonid")
 parser.add_argument("--bam", help="bam file")
 args = parser.parse_args()
 if args.j:
  jrun(args.c,args.s)
 elif args.gap and args.ref and args.bam:
  irun(args.gap,args.ref.args.bam)
 else:
  parser.print_help()
