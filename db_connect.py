#! /usr/bin/env python
#njvarghese 2017
#modules to connect to databases
# img, gold, jamo, spin
import sys
import cx_Oracle
import MySQLdb
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def spin_query(qsql):
 db = MySQLdb.connect("gpdb23","spinaccess","12%c0ttonCandy","spin") #connect to spin ro
 cursor = db.cursor()
 cursor.execute(qsql)
 rval = cursor.fetchall()
 db.close()
 return(rval) 

def spin_write(isql):
 try:
  conn = MySQLdb.connect("gpdb23","spinuser","34%s1deSh0w","spin") # spin rw
  cursor = conn.cursor()
  cursor.execute(isql)
  conn.commit()
 except MySQLdb.Error as error:
  print(error)
 finally:
  cursor.close()
  conn.close()

def imgsg_dev():
 hr = 'imgsg_dev'
 hrpwd = 'Tuesday'
 dsn_tns = cx_Oracle.makedsn('gpodb08.nersc.gov','1521','imgiprd')
 db = cx_Oracle.connect(hr, hrpwd, dsn_tns)
 return(db)

def imgsub_query(qsql):
 db = imgsg_dev()
 cursor = db.cursor()
 cursor.execute(qsql)
 rval = cursor.fetchall()
 db.close()
 return(rval) 

def imgsub_write(isql):
 try:
  db = imgsg_dev()
  cursor = db.cursor()
  cursor.execute(isql)
  db.commit()
 except Error as error:
  print(error)
 finally:
  cursor.close()
  db.close()

def imgcore_query(qsql):
 hr = 'img_core_v400_ro'
## hrpwd = 'img_core_v400_ro987'
 hrpwd = 'freedomSmile2019'
 dsn_tns = cx_Oracle.makedsn('gpodb08.nersc.gov','1521','imgiprd')
 db = cx_Oracle.connect(hr, hrpwd, dsn_tns)
 cursor = db.cursor()
 cursor.execute(qsql)
 rval = cursor.fetchall()
 db.close()
 return(rval)

def jat_query_metadata(params,key):
 r = requests.post('https://sdm2.jgi-psf.org/api/analysis/query', data = params)
 data = r.json()
 if key is None:
  return(data)
 else:
  rval = myprint(data,key)
  return(rval)

def jat_write_metadata(params,key):
 url = "https://sdm2.jgi-psf.org/api/analysis/metadata/" + key
 token = "Application KH40HYJFGA46YFW78VJ41I8WU8RV3D4I"
 auth = {'Authorization' : token}
 r = requests.put(url, headers = auth , data = params)

def jat_query_files(fkey):
 url = 'https://sdm2.jgi-psf.org/api/analysis/analysis/' + fkey + '/files'
 fr = requests.get(url)
 fdata = fr.json()
 return(fdata)

def gold_query_metadata(gparams, gkey):
 goldrval = dict()

 #authentication values
 username = "jgi_neha"
 password = "H^x!S5m"

 burl = 'https://gold.jgi.doe.gov/rest'
 gurl = burl + gparams
 gr =  requests.get(gurl, auth=(username,password) , verify=False)
 
 if gr.status_code == requests.codes.ok: pass
 else: return(None)

 grdata = gr.json()
 dictkeys(grdata,None, goldrval)

 if gkey is None:
  return(goldrval)
 else:
  return(goldrval[gkey])

def keyvalues(val,mkey,rhash):
 if not val: val = None

 if type(val) == type([]): listkeys(val,mkey,rhash)
 elif type(val) == type({}): dictkeys(val,mkey,rhash)
 else: 
  #print str(mkey) + " : " + str(val)
  rhash[mkey] = val

def dictkeys(dref,mkey,rhash):
 for k,v in dref.iteritems():
  if mkey :
   newkey = str(mkey) + "." + k
   keyvalues(v,newkey,rhash)
  else:
   keyvalues(v,k,rhash)

def listkeys(lref,mkey,rhash):
 for v in lref:
  keyvalues(v,mkey,rhash)

#recursive function to parse JSON
#def myprint(d,key):
# for k,v in d.iteritems():
#  if isinstance(v,dict):
#   parsejson(v,key)
#  elif isinstance(v,list):
#   if v:
#    parsejson(v[0],key)
#  else:
#   #print "{0} : {1}".format(k,v)
#   if k == key:
#    return(v)

def gold_ap_metadata(imgtoid):
 toid_params = "/img_taxon/" + str(imgtoid)
 ap_info = gold_query_metadata(toid_params,None)
 return(ap_info)

def gold_sp_metadata(spid):
 sp_params = "/sequencing_project/img/" + str(spid)
 sp_info = gold_query_metadata(sp_params,None)
 return(sp_info)

def gold_study_metadata(studyid):
 study_params = '/study/img/' + str(studyid)
 study_info = gold_query_metadata(study_params, None)
 return(study_info)

def fs_loc(toid, val):
 rval = None
 #base = '/global/dna/projectdirs/microbial/img_web_data_secondary/'
 base = '/global/projectb/sandbox/IMG_web/img_web_data_secondary/'
 if val == 'fna': rval = base + 'taxon.fna/' + str(toid) + '.fna'
 if val == 'faa': rval = base + 'taxon.faa/' + str(toid) + '.faa'
 if val == 'genesfna': rval = base + 'taxon.genes.fna/' + str(toid) + '.fna'
 return(rval)
