#! /usr/bin/env python
import os
import sys
import db_connect
import argparse
import re
import sqlite3

def mgff(imgtoid,ofn):
 odata = []
 sdb_list = []
 mer_dir = '/global/dna/projectdirs/microbial/img_web_data_merfs/' + str(imgtoid) + '/assembled/gene/'
 sdb_query = 'select locus_type,start_coord,end_coord,strand,locus_tag,product_name, gene_oid, scaffold_oid from gene'
 for file in os.listdir(mer_dir):
  if file.endswith(".sdb"):
   sdb_list.append(os.path.join(mer_dir, file))

 for sdb in sdb_list:
  sys.stderr.write(sdb + "\n")
  conn = sqlite3.connect(sdb)
  c = conn.cursor()
  c.execute(sdb_query)
  for row in c.fetchall():
   locus_type,start,end,strand,locus_tag,product_name, goid, scaffold_oid = row
   #attributes = "ID=" + str(goid) + ";Name=" + str(product) + ";gene_id=" + str(locus_tag) + ";gene_name=" + str(locus_tag)
   #attributes+= ";gene_oid=" + str(goid) + ";scaffold_oid=" + str(scaffold_oid) + ";product_name=" + str(product)
 
   attributes = "ID=" + str(goid) + ";gene_id=" + str(goid) + ";locus_tag=" + str(locus_tag) + ";scaffold_oid=" + str(scaffold_oid)
   odata.append([scaffold_oid,"img_core",locus_type,str(start),str(end),".",strand,"0", attributes])
   
  conn.close()
 
 tofile(odata,ofn)

def tofile(odata,ofn):
 fh = open(ofn,'w')
 for edata in odata:
  line = "\t".join(edata)
  fh.write(line + "\n")
 fh.close()

def igff(imgtoid,ofn):
 odata = []
 source = []
 sql = 'select g.locus_type,g.start_coord,g.end_coord, g.strand, g.locus_tag, g.product_name,g.gene_oid'
 sql+= ',s.ext_accession, s.scaffold_oid,ss.SEQ_LENGTH, g.cds_frag_coord from gene g'
 sql+= ' join scaffold s on s.scaffold_oid=g.scaffold'
 sql+= ' join SCAFFOLD_STATS ss on ss.SCAFFOLD_OID=g.SCAFFOLD'
 sql+= ' where g.taxon = ' + str(imgtoid)+ ' order by s.ext_accession, g.start_coord, g.end_coord'

 rval = db_connect.imgcore_query(sql)
 counter = 0
 for line in rval:
  locus_type,start,end, strand, locus_tag, product, goid,scaffold, scaffold_oid, scaffoldSize, frags = line
  scaffold = scaffold.replace(';','')
  fragments = []

  if locus_tag : locus_tag = locus_tag.replace(';','')
  else:
   counter += 1 
   locus_tag = scaffold + "." + str(counter)

  if product : product = product.replace(';','')
  else :  product = ""

  if frags : 
   pat = re.compile('complement|join|\(|\)')
   c_frags = re.sub(pat,'',frags)
   each_frag = c_frags.split(',')
   for efrag in each_frag:
    start,stop = efrag.split('\.\.')
    fragments.append([str(start),str(stop)])
   
  if scaffold not in source:
   odata.append([scaffold,"img_core","source","1",str(scaffoldSize),".","+","0","contig_len="+str(scaffoldSize)])
   source.append(scaffold) 

  #attributes = "ID=" + str(goid) + ";Name=" + str(product) + ";gene_id=" + str(locus_tag) + ";gene_name=" + str(locus_tag)
  #attributes+= ";gene_oid=" + str(goid) + ";scaffold_oid=" + str(scaffold_oid) + ";product_name=" + str(product)
  attributes = "ID=" + str(goid) + ";locus_tag=" + str(locus_tag) + ";scaffold_oid=" + str(scaffold_oid)

  odata.append([scaffold,"img_core",locus_type,str(start),str(end),".",strand,"0", attributes])

  if len(fragments) > 1 :
   frag_num = 0
   for efrag in fragments:
    frag_num += 1
    #attributes = "Parent=" + str(goid) + ";Name=" + str(product) + ";gene_id=" + str(locus_tag) + "." + str(frag_num) + ";gene_name=" + str(locus_tag) + "." + str(frag_num)
    #attributes+= ";gene_oid=" + str(goid) + ";product_name=" + str(product)
    attributes = "Parent=" + str(goid) + ";gene_id=" + str(goid) +";locus_tag=" + str(locus_tag) + ";scaffold_oid=" + str(scaffold_oid)  

    odata.append([scaffold,lc(dbhname),"exon",efrag[0],efrag[1],".",strand,"0",attributes])

 tofile(odata,ofn)

def mfna(imgtoid,ofn):
 odata = ''
 sdb_list = []
 #mer_dir = '/global/dna/projectdirs/microbial/img_web_data_merfs/' + str(imgtoid) + '/assembled/fna/'
 mer_dir = '/global/projectb/sandbox/IMG/img_web_data_merfs/' + str(imgtoid) + '/assembled/fna/'
 sdb_query = 'select scaffold_oid, fna from scaffold_fna'
 for file in os.listdir(mer_dir):
  if file.endswith(".sdb"):
   sdb_list.append(os.path.join(mer_dir, file))

 for sdb in sdb_list:
  sys.stderr.write(sdb + "\n")
  conn = sqlite3.connect(sdb)
  c = conn.cursor()
  c.execute(sdb_query)
  for row in c.fetchall():
   oid,seq = row
   odata += '>' + str(oid) + '\n' + seq
  conn.close()

 wh = open(ofn,'w') 
 wh.write(odata)
 wh.close()

def mfaa(imgtoid,ofn):
 odata = ''
 sdb_list = []
 mer_dir = '/global/dna/projectdirs/microbial/img_web_data_merfs/' + str(imgtoid) + '/assembled/faa/'
 sdb_query = 'select gene_oid, faa from gene_faa '
 for file in os.listdir(mer_dir):
  if file.endswith(".sdb"):
   sdb_list.append(os.path.join(mer_dir, file))

 for sdb in sdb_list:
  sys.stderr.write(sdb + "\n")
  conn = sqlite3.connect(sdb)
  c = conn.cursor()
  c.execute(sdb_query)
  for row in c.fetchall():
   oid,seq = row
   odata += '>' + str(oid) + '\n' + seq
  conn.close()

 wh = open(ofn,'w')
 wh.write(odata)
 wh.close()

def erfna(imgtoid,ofn):
 er_fn = '/global/dna/projectdirs/microbial/img_web_data/taxon\.fna/' + str(imgtoid) + 'taxon\.fna'
 with open(er_fn,'r') as content_file:
  content = content_file.read()

 wh = open(ofn,'w')
 wh.write(content)
 wh.close()

if __name__ == '__main__':
 parser = argparse.ArgumentParser(description='IMG filesystem based functions')
 parser.add_argument("func" , help = "igff|mgff|mfna")
 parser.add_argument("imgtoid", help = "IMG taxon ID")
 parser.add_argument("ofn", help ="name of output file")
 args = parser.parse_args()
 if args.func == "igff" :  ergff(args.imgtoid,args.ofn) 
 if args.func == "mgff" :  mgff(args.imgtoid,args.ofn)
 if args.func == "ifna" :  erfna(args.imgtoid,args.ofn)
 if args.func == "mfna" :  mfna(args.imgtoid,args.ofn)
