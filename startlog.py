#! /usr/bin/env python
from time import gmtime, strftime
import logging

def run(name):
 date = strftime("%Y%m%d", gmtime())
 logfn = 'gea.' + name + '.' + str(date) + '.log'
 logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y || %I:%M:%S %p || ',filename=logfn,level=logging.INFO)
 logging.getLogger("requests").setLevel(logging.WARNING) #not log messages unless they are at least warnings
 logging.getLogger().addHandler(logging.StreamHandler())
 return(logging)

