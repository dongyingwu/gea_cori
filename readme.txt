######
this repo has been superseded by gea_cori_100Gmem
#####




























GEA - gene expression analysis
RNAseq expression data analysis in IMG

dongying wu 2019
update for cori.nersc

the process needs the script directory (/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/), two conda 
enviroments(/global/homes/d/dywu/anaconda2/envs/gbs and /global/homes/d/dywu/anaconda2/envs/rnaseq)

####################
Update steps:
 
1. update conda envirements

git clone https://dongyingwu@bitbucket.org/njvarghese/gea-denovo.git
cp gea-denovo/* /global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/.

~dywu/anaconda2/bin/conda create -p /global/homes/d/dywu/anaconda2/envs/gbs --clone /global/projectb/scratch/varghese/DENOVO/conda_bin/gbs
source ~dywu/anaconda2/bin/activate /global/homes/d/dywu/anaconda2/envs/gbs
conda install -c anaconda oracle-instantclient=11.2.0.4.0

~dywu/anaconda2/bin/conda create -p /global/homes/d/dywu/anaconda2/envs/rnaseq --clone /global/projectb/scratch/varghese/DENOVO/conda_bin/rnaseq
source ~dywu/anaconda2/bin/activate /global/homes/d/dywu/anaconda2/envs/rnaseq
conda install -c bioconda samtools

2. update rnaseq_gea.py so it works on cori
rnaseq_gea.py requires ~dywu/anaconda2/bin/activate and /global/homes/d/dywu/anaconda2/envs/rnaseq 

3. get write permission from Neha Varghese to the directory: /global/projectb/sandbox/omics/rnaseq/Metatranscriptomes/ 

##################

Example runs on cori:

(rnaseq_img run on cori)
source ~dywu/anaconda2/bin/activate /global/homes/d/dywu/anaconda2/envs/gbs
export PYTHONPATH=$PYTHONPATH:/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm
/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/rnaseq_img.py -j

(check cori run results)
/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/check_cori_runs.py

(follow the instructions given by check_cori_runs.py, resubmit via esslurn if necessary) 
module load esslurm
/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/resubmit_via_esslurm.py

(submit to img)
source ~dywu/anaconda2/bin/activate /global/homes/d/dywu/anaconda2/envs/gbs
export PYTHONPATH=$PYTHONPATH:/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm
/global/homes/d/dywu/metatranscriptome_dir/gea_cori_need_esslurm/rnaseq_img_submit.py

#######################

njvarghese 2016,2017

connect.py : connect to SDM
readCov_metaTranscriptome.pl : Find Cov (+ strand) and Cor (- strand) per gene for MetaTransciptomics (Ed's script ,modified by kbillis[at].gov)
rnaseq_at_status.py : updates the AT status to 6
rnaseq_jamo.py: JAMO pickup and checks of releases with template 'metatranscriptome_alignment' and 'metatranscriptome_assembly_and_alignment' 
startlog.py: generate log file
rnaseq_gea.py: checks metadata and generates the sh file for cluster job

Wrappers:
rnaseq_img.py: calls rnaseq_jamo for release pickup and sends each to rnaseq_gea
rnaseq_img_submit.py: checks current folder to completed analysis and gathers metadata for submission to IMG submission database
