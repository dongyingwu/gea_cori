#!/usr/bin/env perl 
use strict;
use warnings;
use bytes;
use Getopt::Long;

#last update : Sep 3rd, 2013 : reflect specifications in Manoj's input spec
#				cleanup script to remove unused variables

# INIT VARS
my ($help, $ref_infile, $sam_infile, $gff_file, $genes_outfile, $map_names_file, $lib, $experiment_oid,$ref_type); #$sample_oid
#my $samtools="/usr/common/jgi/file_formats/samtools/0.1.18/bin/samtools";
#system("module load samtools");

# GET OPTIONS
GetOptions(
    'lib=s'    => \$lib,
    #'eid=s'    => \$experiment_oid,
    #'sid=s'    => \$sample_oid,
    'ref=s'    => \$ref_infile,
    'gff=s'    => \$gff_file,
    'sam=s'    => \$sam_infile,
    'exp=s'    => \$genes_outfile,
    'map=s'    => \$map_names_file,
    'type=s'   => \$ref_type,
    'h|help'   => \$help
);


my $usage ="
PURPOSE
    Find Cov (+ strand) and Cor (- strand) per gene for MetaTransciptomics (NO alternative splicing)
    Say directional or non-directional libraries
INPUT
    --lib <> : directional or non-directional
    --sid <> : sample id, 
    --ref <> : ref_infile,
    --gff <> : annotation file,
    --sam <> : sam file or bam file
    --map <> : map previous names with new
    --exp <> : name of output file
    --type <> : database type of reference (IMG/MER or IMG ER)

OUTPUT
    --exp <> : outfile, counts per gene

BY
    (Ed's script ,modified by kbillis[at].gov , cleaned up by njvarghese)
";

die ($usage) if (!$ref_infile or !$sam_infile or !$gff_file or !$genes_outfile or !$lib); #or !$sample_oid);
die ($usage) if ($help);

# INIT OBJECT
print "#Initializing object..\n";
my $read_cov = new ReadCov($ref_infile, $map_names_file);    # object uses 0-based coordinates
print "#scaffold and name_map files were loaded\n";

# LOAD ALIGNMENTS
# there are two case if you like to take into account XA (multiple hits) or not
$read_cov->read_sam_fileXA($sam_infile) if $sam_infile;
# $read_cov->read_sam_file($sam_infile) if $sam_infile;
print "#sam/bam file processed\n";

# GENE OUTPUT
$read_cov->write_gene_cov_tsv($gff_file, $genes_outfile, $experiment_oid, $lib ); #$sample_oid

print "#Counting succesfuly completed\n";

# OBJECT

package ReadCov;

# objects are blessed arrays, not hashes.
use constant {
    SEQ => 0,    # hash of chrom ID => nucleotide sequence
    LEN => 1,    # hash of chrom ID => chrom length (in bp)
    COV => 2,    # hash of chrom ID => read coverage per base
    RDS => 3,    # hash of chrom ID => number of reads (each read contributes at a single point)
    COR => 4,	 # reverse of COV
    RDR => 5,	 # reverse of RDS
    MAP => 6	 # previous names/ids of the loaded sequences (usually assembly group uses them)     
};

use PDL;
use PDL::IO::Misc;
use PDL::IO::FastRaw;
use PDL::NiceSlice;
use IO::File;

###############################################################################
## INIT
sub new{
	my ($class, $infile, $mapfile) = @_;
	my $this = [ {}, {}, {}, {}];
  	bless $this, $class;
	$this->read_fasta($infile) if $infile;
	$this->mapNames($mapfile) if $mapfile;
	return $this;
}

###############################################################################
# LOAD SEQUENCE DATA
sub read_fasta{
  my ($this, $infile) = @_;
  my ($chr, $seq) = ('', '');
  open(FASTA, "<$infile") or die("ERROR: Cannot open $infile\n");
  while (<FASTA>) {
    chomp;
    if (/^>(\S+)/) {
      $this->_save_chr($chr, $seq);
      my @d=split(/ /,$1);
      ($chr, $seq) = ($d[0], '');
      #print "Fna: $chr\n";
    }else{
      $seq .= $_;
    }
  }
  $this->_save_chr($chr, $seq);
  close FASTA;
  return
}

sub _save_chr{
  my ($this, $chr, $seq) = @_;
  return unless $chr and $seq;
  $this->[SEQ]->{$chr} = uc($seq);
  my $len = $this->[LEN]->{$chr} = length($seq);
  $this->[COV]->{$chr} = zeroes($len);
  $this->[COR]->{$chr} = zeroes($len);

  $this->[RDS]->{$chr} = zeroes($len);
  $this->[RDR]->{$chr} = zeroes($len);
}

# find previous names for the selected sequences
sub mapNames {
  my ($this, $file1) = @_;
  my %hashRename;
  if($file1){
    if (-e $file1){
      print "Map File Exists!\n";
      open(IN, "<$file1") or die("ERROR: Cannot open $file1\n");
      while (<IN>) {
        chomp;
	my @line=split(/\t/,$_);
	#if ($this->[LEN]->{$line[1]}) {
	  $this->[MAP]->{$line[1]} =$line[0];	# uncomment those lines			
	#} 
      }
      close IN;
    }else{
      print "Map file defined..however it does not exist:\n please check file : $file1\n";
    }
  }else{
    print "There isn't a file to map names\n";
  }
  return %hashRename;	
} 


#	take into account XA		#
sub read_sam_fileXA {
  my ($this, $infile)=@_;
  my $prev_read_id='';
	
  my %map_names;
  foreach my $chr (keys %{$this->[MAP]}) {
    my $mapId  = $this->[MAP]->{$chr};
    $map_names{$mapId} = $chr; 
  }

  print "Reading $infile\n";

  #open(my $fh,"$samtools view -h '$infile' |") or die "Cannot open $infile $!";
  #open(my $fh,"samtools view $infile |") or die "Cannot open $infile $!\n";
  #while(<$fh>) {
  my $cmd=qq{samtools view $infile};
  open(my $fh, "-|", $cmd) or die $!; 
  while(<$fh>){
    chomp;next if !length($_);
    my $line=$_;
    #print "LINE: $line\n";
    next if ($line=~/^@/); # header section
		
    #print "BAM: $line\n";
    my @row=split(/\t/,$line);
    my $read_id=$row[0];
    my $flag=$row[1];
    #my $ref_id=$row[2];
    my @split_up=split(/\s+/,$row[2]);
    my $ref_id=$split_up[0];
    my $strand= $flag & (1 << 4) ? '-':'+';
    my $not_ = $flag & (1 << 2) ?  'no': 'yes';
    next if $not_ eq "no";
    next if $ref_id eq '*'; # unaligned

    #print "read_id: $read_id\tref_id: $ref_id\n";die;	
    if ( $map_names{$ref_id} ) {
      $ref_id = $map_names{$ref_id};
      #print "Map:ref_id: $ref_id\n";
    }
    unless (exists($this->[COV]->{$ref_id}) ) {  
      print "No coverage for $ref_id\n";  
      next;  
    } 
        	
    my $cigar=$row[5];
    my $len=ref_length($cigar);
    #print qq{Length of reference: $len \n};
    my $left = $row[3]-1; # convert to 0-based coord system
    my $right=$left + $len - 1; # convert to 0-based coord system
    my $x= 1;
    #print qq{#$read_id\t$ref_id\t$len\t$strand\t$left\t$right\n};	
    $this->add_read($ref_id, $strand, [$left],[$right], $x);
    my @XA=();
    if(/XA:Z:/){
      #split all alternative reads
      my @line = split(/XA:Z:/);
      push(@XA, @line[1..$#line]);
      foreach my $multiRead (@XA) {
        if (!defined($multiRead)) {next;}
	my @alterReadXA = split(/;/, $multiRead);			
	foreach my $alterRead (@alterReadXA) {
	  my @readXA = split(/,/, $alterRead);
	  $ref_id = $readXA[0];
	  my $pos = $readXA[1];
	  if ($pos =~ /[+]/ ) {
	    $strand = "+";
	    $pos =~ s/[+-]//g;
	    my $startPos = $pos;
	    $cigar=$row[5];
	    $len=ref_length($cigar);
	    #$len=ref_lengthXA($cigar);
	    $left = $startPos;
	    $right=$left + $len - 1; # convert to 0-based coord system
	    $x=1; # at this point and before I could change the value- weight of the read
	    $this->add_read($ref_id, $strand, [$left],[$right], $x);
	  }elsif ($pos =~ /[-]/ ) {
	    $strand = "-";
	    $pos =~ s/[+-]//g;
	    my $startPos = $pos;
	    $cigar=$row[5];
	    $len=ref_length($cigar);
	    #$len=ref_lengthXA($cigar);
	    $left = $startPos;
	    $right=$left + $len - 1; # convert to 0-based coord system
	    $x=1; # at this point and before I could change the value- weight of the read
	    $this->add_read($ref_id, $strand, [$left],[$right], $x);
	  }else { print " problem with $read_id";}
        }
      }
    }
  #die;
  }
  close IN;
  print "Successful in reading BAM file\n";
}

sub ref_length {
  my $fullcigar=shift;
  my $len=0;
  my $cigar=$fullcigar;
  while($cigar) {
    #if ($cigar =~ /^(\d+)([MIDNSHP])(.*)$/) { #billis
    if ($cigar =~ /^(\d+)([MIDNSHPX=])(.*)$/) {
      $cigar=$3;
      unless ($2 eq 'I' or $2 eq 'S' or $2 eq 'H') { $len += $1 ? $1:1 } #billis
    }else{
      print("Unable to parse cigar string, \"$fullcigar\" part : $cigar\n");
      $cigar=undef;
    }
  }
  return $len;
}
=cut
sub ref_lengthXA {
  my $fullcigar=shift;
  my $len=0;
  my $cigar=$fullcigar;
  while($cigar) {
     if ($cigar =~ /^(\d+)([MIDNSHP=])(.*)$/) {
       $cigar=$3;
       unless ($2 eq 'I' or $2 eq 'S' or $2 eq 'H') { $len += $1 ? $1:1 }
       } else {
        #die("Unable to parse cigar string, \"$fullcigar\"\n");
       print ("Unable to parse cigar string, \"$fullcigar\" part : $cigar\n");
       $cigar=undef; 
    }
  }
  return $len;
}
=cut

#Save a read's alignment.  Note: 0-based coordinates are used.
sub add_read{
    my ($this, $chr, $strands, $starts, $ends, $x) = @_;

    # VALIDATE INPUT    
    die("Invalid chr, $chr\n")    unless exists($this->[COV]->{$chr});
    die("Invalid chr, $chr\n")    unless exists($this->[COR]->{$chr});
    die("Invalid starts array\n") unless @$starts;
    die("Invalid ends array\n")   unless @$ends;
    $x = 1 unless $x;    # used for adding several identical reads at once (optional; default to 1)

    # INCREMENT COVERAGE VECTOR FOR EACH EXON LOCUS
    my $len=0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
      my $start = $starts->[$i];
      my $end= $ends->[$i];
      unless ($start >= 0) {
        print "Warning: Invalid start coordinate $start for the read on contig, $chr (" . $this->[LEN]->{$chr} . " bp)\n";
        $start=0;
      }
      unless ($start < $this->[LEN]->{$chr}) {
        print "Waring: Invalid start coordinate $start for read for contig, $chr (" . $this->[LEN]->{$chr} . " bp).End is ".$ends->[$i]."\n"; 
        next;
      }
      
      if ( $end >= $this->[LEN]->{$chr} ) {
        print "Warning: Invalid end coordinate $end for the read on contig $chr (" . $this->[LEN]->{$chr} . " bp)\n";
        $end = $this->[LEN]->{$chr} - 1;
      }

      if ($strands eq "+"){
        $this->[COV]->{$chr}->($start : $end) += $x;
        $len += ($end-$start+1);
        #print "$strands\t$start\t$end\n";
      }elsif ($strands eq "-"){
      	$this->[COR]->{$chr}->($start : $end) += $x;
        $len += ($end-$start+1);
		  #print "$strands\t$start\t$end\n"
      }

    }

    # INCREMENT READ COUNTER
    # choose midpoint
    my $half = int($len/2);
    my $pos;
    for (my $i=0; $i<=$#$starts; $i++) {
      my $start = $starts->[$i];
      my $end = $ends->[$i];
      
      unless ($start < $this->[LEN]->{$chr}) {
        print "Invalid start coordinate, $start, for contig, $chr (" . $this->[LEN]->{$chr} . " bp).End is ".$ends->[$i]."\n";
        next;
      }

      my $len = $end-$start+1;
      if ($half < $len) { # midpoint is in this block
        $pos = $start + $half;
      }else{
        $half -= $len;
      }
    }

    if ($strands eq "+"){
      $this->[RDS]->{$chr}->($pos) += $x;
	    #print "$strands\t$start\t$end\n";
    }
    elsif ($strands eq "-"){
      $this->[RDR]->{$chr}->($pos) += $x;
	    #print "$strands\t$start\t$end\n";
    }
}

=head2 locus_coverage_stats

Returns coverage stats for a locus/gene.

=cut

sub locus_coverage_stats{
    my ($this, $chr, $starts, $ends) = @_;
    die("Chromosome ($chr) does not exist!\n") unless exists($this->[LEN]->{$chr});
    if ($ends->[$#$ends] >= $this->[LEN]->{$chr}) {
        print "Warning: Locus ends as position ".$ends->[$#$ends].", beyond end of $chr (".$this->[LEN]->{$chr}." bp)\n";
        $ends->[$#$ends] = $this->[LEN]->{$chr} - 1;
    }
    my $tot_len = 0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
        $tot_len += ($ends->[$i] - $starts->[$i] + 1);
    }
    my $chr_cov  = $this->[COV]->{$chr};
    my $chr_cor  = $this->[COR]->{$chr};

    my $chr_rds  = $this->[RDS]->{$chr};
    my $chr_rdr  = $this->[RDR]->{$chr};

    my $gene_cov = zeroes($tot_len);
    my $gene_cor = zeroes($tot_len);

    my $gene_rds = zeroes($tot_len);
    my $gene_rdr = zeroes($tot_len);

    my $start    = 0;
    my $end      = 0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
        my $ex_start = $starts->[$i];
        my $ex_end = $ends->[$i];
        my $ex_len = $ex_end - $ex_start + 1;
        $end += ($ex_len - 1);
        $gene_cov ($start : $end) .= $chr_cov ($ex_start : $ex_end);
		    $gene_cor ($start : $end) .= $chr_cor ($ex_start : $ex_end);
        $gene_rds ($start : $end) .= $chr_rds ($ex_start : $ex_end);
        $gene_rdr ($start : $end) .= $chr_rdr ($ex_start : $ex_end);
        $start = ++$end;
    }

    my $reads  = sum($gene_rds       (0 : $tot_len - 1));
    my $readsA  = sum($gene_rdr       (0 : $tot_len - 1));

    my $median = oddmedian($gene_cov (0 : $tot_len - 1));
    my $medianA = oddmedian($gene_cor (0 : $tot_len - 1));

    my ($mean, $stddev) = stats($gene_cov (0 : $tot_len - 1));
    my ($meanA, $stddevA) = stats($gene_cor (0 : $tot_len - 1));

    $mean   = int($mean * 100 + 0.5) / 100;
    $meanA   = int($meanA * 100 + 0.5) / 100;

    $stddev = int($stddev * 100 + 0.5) / 100;
    $stddevA = int($stddevA * 100 + 0.5) / 100;

    return ($tot_len, $reads, $median, $mean, $stddev, $readsA, $medianA, $meanA, $stddevA);
}

sub write_gene_cov_tsv {
  my ($this, $infile, $outfile, $experiment_oid, $lib ) = @_; #$sample_oid
	
  open(OUT,">$outfile")or die$!;
  print OUT qq{img_gene_oid\timg_scaffold_oid\tlocus_tag\tscaffold_accession\tstrand\tlocus_type\tlength\treads_cnt\tmean\tmedian\tstdev\treads_cntA\tmeanA\tmedianA\tstdevA\n}; #sample\n};
	
  #open(OUT, ">$outfile") or die($!);
  #print OUT "scaffold_accession\tlocus_tag\tstrand\tlocus_type\tlength\treads_cnt\tmean\tmedian\tstdev\treads_cntA\tmeanA\tmedianA\tstdevA\texperiment\tsample\n";

  open(IN,$infile)  or die($!);
  while (<IN>) {
    chomp;
    next if /^##/; # header section
	
    my ($chr,$src,$type,$start,$end,$score,$strandG,$phase,$comment)=split(/\t/);
    next if(/^#/);
    unless($type=~/CDS/i){next;}
    
    #unless($this->[COV]->{$chr}){
    unless (exists($this->[COV]->{$chr})) { 
     print "No coverage found for chromosome $chr\n";
     next;
    }

    $start  =~ s/\>//g;
    $start  =~ s/\<//g;

    $end  =~ s/\>//g;
    $end  =~ s/\<//g;
		
    $strandG = "+" if ($strandG eq "1");
    $strandG = "-" if ($strandG eq "-1");

    my @prev_starts = ();
    my @prev_ends =();
		
    push @prev_starts, $start;
    push @prev_ends, $end;
		
    $comment =~m/;locus_tag=(.*?);/s;
    my $locus_tag =$1;
		
    $comment =~m/;scaffold_oid=(.*?);/s;
    my $img_scaffold_oid =$1;
		
    $comment =~m/ID=(.*?);/s;
    my $img_gene_oid =$1;
    #if($img_gene_oid=~/^JGI/ || $ref_type eq "IMG/MER"){
    #	$img_gene_oid=$locus_tag;
    #}
	
    unless(defined $locus_tag && defined $img_scaffold_oid && defined $img_gene_oid){
      #print "Checking for different format of GFF file\n";
      $comment =~m/;gene_name=(.*?);/s;
      $locus_tag =$1;

      $comment =~m/;scaffold_oid=(.*?);/s;		
      $img_scaffold_oid =$1;

      $comment =~m/gene_oid=(.*?);/s;
      $img_gene_oid =$1;
    }

    unless(defined $locus_tag && defined $img_scaffold_oid && defined $img_gene_oid){
      print "Format of GFF file does not match required format. Please check file : $infile\n";
      exit;
    }

    my ($len, $reads, $median, $mean, $stddev, $readsA, $medianA, $meanA, $stddevA) = $this->locus_coverage_stats($chr, \@prev_starts, \@prev_ends);
    next if  ( $reads == 0 );

    if ( $lib eq "directional") {
      # when the libraries are directional / strand specific
      if ($strandG eq "+") {
        #print OUT join("\t", $chr, $geneID, $strandG, $type, $len, $readsA, $meanA, $medianA, $stddevA, $reads, $mean, $median, $stddev,  $experiment_oid , $sample_oid), "\n";
        print OUT join("\t",$img_gene_oid,$img_scaffold_oid,$locus_tag,$chr,$strandG, $type, $len, $readsA, $meanA, $medianA, $stddevA, $reads, $mean, $median, $stddev), "\n";
    
      } elsif ($strandG eq "-") {	
        #print OUT join("\t", $chr, $geneID, $strandG, $type, $len, $reads, $mean, $median, $stddev, $readsA, $meanA, $medianA, $stddevA, $experiment_oid , $sample_oid ), "\n";
        print OUT join("\t",$img_gene_oid,$img_scaffold_oid,$locus_tag,$chr,$strandG, $type, $len, $reads, $mean, $median, $stddev, $readsA, $meanA, $medianA, $stddevA), "\n";
      } else { 
        print "problem\n";exit;	
      }
					
    }else{
      $reads =$reads + $readsA;
      $readsA = $medianA = $meanA = $stddevA = $median = $mean = $stddev = "0";

      #print OUT join("\t" , $chr, $geneID, $strandG, $type, $len, $reads, $mean , $median , $stddev, $readsA, $meanA, $medianA, $stddevA, $experiment_oid , $sample_oid), "\n";
      print OUT join("\t",$img_gene_oid,$img_scaffold_oid,$locus_tag,$chr,$strandG, $type, $len, $reads, $mean , $median , $stddev, $readsA, $meanA, $medianA, $stddevA),"\n";
    }
  }	
  close IN;
  close OUT;	
}


__END__


