#! /usr/bin/env python
import os,sys
import requests, json
from requests.auth import HTTPBasicAuth
from requests import Request, Session

pmos_base = 'http://proposals.jgi.doe.gov/pmo_webservices/'
token = 'token=a0fa21c69552f44bb338b630fbf37a2d'

def at_info(sp_id):
 #get AP for 'RNA expression' task corresponding to a given SP
 at_type_id = 12 #http://proposals.jgi.doe.gov/pmo_webservices/analysis_task_types
 at_url = pmos_base + 'analysis_tasks?sequencing_project_id=' + str(sp_id) + '&analysis_task_type_id=' + str(at_type_id)
 print at_url

 r = requests.get(at_url)
 if r.status_code == requests.codes.ok: pass
 else: sys.exit('Error code' + str(r.status_code) +'\n')

 rdata = r.json()

 for main in rdata['uss_rw_analysis_tasks']:
  if main['analysis_task_type_name'] == 'RNA Expression':
   if (main['current_status_id'] == 1  or main['current_status_id'] == 5 or main['current_status_id'] == 8):
    #print main['analysis_project']['analysis_project_id']
    return(main['analysis_project']['analysis_project_id'])

def ap_info(corr_ap_id): #added for combined assembly's May 22nd 2018
 #takes as input an ap id within the same FD
 fd_url = pmos_base + 'its_project/' + str(corr_ap_id)
 
 #default values
 has_exp_task = 'no'
 req_ap_id = None

 fd_r = requests.get(fd_url)
 if fd_r.status_code == requests.codes.ok: pass
 else: sys.exit('Error code' + str(fd_r.status_code) +'\n')

 fd_rdata = fd_r.json()

 for fd_main in fd_rdata['final_deliverable_project']['analysis_projects']:
  ap_type_name = fd_main['analysis_product_type_name']
  #print ap_type_name
  if ap_type_name.startswith('Metagenome Metatranscriptome Expression') or ap_type_name.startswith('Eukaryote Community Metatranscriptome Expression') or ap_type_name.startswith('Co-Assembly - Eukaryotic Community Metatranscriptome Expression') :
   req_ap_id = fd_main['analysis_project_id']

   ap_url = pmos_base + 'analysis_project/' + str(req_ap_id)
   #print ap_url
   ap_r = requests.get(ap_url)
   if ap_r.status_code == requests.codes.ok: pass
   else: sys.exit('Error code' + str(r.status_code) +'\n')
   ap_rdata = ap_r.json()

   for main in ap_rdata['uss_analysis_project']['analysis_tasks']:
    task_name = main['analysis_task_type_name']
    #print task_name
    task_status_id = main['current_status_id']
    if task_name == "RNA Expression" :
     #print task_status_id
     if (task_status_id == 1) or (task_status_id == 5) or (task_status_id == 8) :
     #if (task_status_id == 5) :
      has_exp_task = 'yes'
      if req_ap_id and has_exp_task == 'yes': return(req_ap_id)
 
def update_at_status(ap_id):
 ap_url = pmos_base + 'analysis_project/' + str(ap_id)
 print ap_url

 r = requests.get(ap_url)
 if r.status_code == requests.codes.ok: pass
 else: sys.exit('Error code' + str(r.status_code) +'\n')

 rdata = r.json()

 for main in rdata['uss_analysis_project']['analysis_tasks']:
  task_name = main['analysis_task_type_name'] 
  task_status = main['status_name']
  task_status_id = main['current_status_id']
  task_id =  main['analysis_task_id']
  #print "checking:" + str(task_name) + "\t" + str(task_status) + "\t" + str(task_id)
  if task_status_id == 6 : continue
  if not task_name == "RNA Expression" : continue
  print "Updating " + str(task_name) + "\t" + str(task_status) + "\t" + str(task_id)

  #update the task
  update_url = 'https://proposals.jgi.doe.gov/pmo_webservices/analysis_task/' + str(task_id) + '/state_transition'
  #print update_url
  aheaders = {'Content-type': 'application/json','Authorization':'Token token=a0fa21c69552f44bb338b630fbf37a2d'}
  params = {"updated-by-cid": "5457","target-state-id":6}
  ur = requests.post(update_url, data=json.dumps(params), headers=aheaders)
  if ur.status_code == 204: pass
  else:
   print ur.headers
   print ur.status_code
   sys.exit(1)

if __name__ == '__main__':
 ap = sys.argv[1]
 req_ap = ap_info(ap)
 if req_ap : print req_ap
 else: print "Not found"
