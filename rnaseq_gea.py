#! /usr/bin/env python
import os,sys,subprocess
import img_fs_funcs

def run(gap,reftaxon,bamfn,maptype,logging,jat_key_id):
 gfffn = mapfn = None
 odir = gap
 path = os.path.abspath(os.path.dirname(__file__))
 if os.path.isdir(odir): 
  sys.stderr.write(odir + " exists. Skip\n")
  return()
 os.mkdir(odir)
 #tmpsam = "modified.sam"
 #tmpbam = "modified.bam"
 #tmpout = "featurecount.out"
 jobsh = odir + "/" + str(gap) + "_gea.sh"
 jobwd = os.getcwd() + "/" + odir
 metafn = odir + "/metadata.tab"
 tar_loc = '/global/dna/projectdirs/microbial/img_web_data_secondary/download/' + str(reftaxon) + '.tar.gz'
 if reftaxon > 3000000000: tar_loc = '/global/dna/projectdirs/microbial/img_web_data/mer.fs/download/'+ str(reftaxon) + '.tar.gz'
 readcov_script_dir = dir_path = os.path.dirname(os.path.realpath(__file__))

 #gff
 gffloc = odir + "/" + str(reftaxon) + '.fs.gff'
 gfffn = str(reftaxon) + '.fs.gff'
 #sys.stderr.write("creating gff: " + gffloc + "\n")
 logging.info("creating gff: " + gffloc)
 if reftaxon > 3000000000: img_fs_funcs.mgff(reftaxon,gffloc)
 else: img_fs_funcs.igff(reftaxon,gfffn)
 
 #fna
 fnaloc = odir + "/" + str(reftaxon) + '.fs.fna'
 fnafn = str(reftaxon) + '.fs.fna'
 if reftaxon > 3000000000: 
  fnafiles = subprocess.check_output(['tar','-xf', tar_loc, '-C', odir, '-v', '--wildcards', '*.a.fna'])
  fnafn = fnafiles.rstrip()
  fnaloc = odir + "/" + fnafn
  if os.path.isfile(fnaloc): logging.info("fna file found:" + fnaloc)
  else: 
   logging.info("creating fna: " + fnaloc)
   img_fs_funcs.mfna(reftaxon,fnaloc)
 else: img_fs_funcs.ifna(reftaxon,fnaloc)

 #map
 if maptype in ['self','other']:  ##modified on 6/10/2019
  mapfiles = subprocess.check_output(['tar','-xf', tar_loc, '-C', odir, '-v', '--wildcards', '*.map*'])
  mapfn = mapfiles.rstrip()
  #sys.stderr.write("map file found " + mapfn + "\n")
  logging.info("map file found " + mapfn)
 

 #make job file : modified by dongying on 6/13/2019, and 8/12/2019
 jo =  "#!/bin/bash\n"
 jo += "#SBATCH --qos=genepool \n"
 jo += "#SBATCH -D " + jobwd + "\n"
 jo += "#SBATCH -N 1\n"
 jo += "#SBATCH --mem=100G\n"
 jo += "#SBATCH -t 12:00:00\n"
 # jo += "#SBATCH --mail-user=njvarghese@lbl.gov\n"
 # jo += "#SBATCH --mail-type=FAIL\n"
 # jo += "module load samtools\n"
 # jo += "source activate /global/projectb/scratch/varghese/DENOVO/conda_bin/rnaseq\n"
 jo += "source ~dywu/anaconda2/bin/activate /global/homes/d/dywu/anaconda2/envs/rnaseq\n"  
 ## ADD SOURCE ACTIVATE RNASEQ of current 
 jo += "/usr/bin/time " + readcov_script_dir + "/readCov_metaTranscriptome.pl"
 jo += " -ref " + fnafn
 jo += " -gff " + gfffn
 jo += " -sam " + bamfn
 jo += " -exp rnaseq_gea.tab -lib directional -type IMG/MER"
 if mapfn: jo += " -map " + mapfn
 jo += "\n"
  
 fh = open(jobsh,'w')
 fh.write(jo)
 fh.close()

 #submit to cluster
 subprocess.check_call(["sbatch",jobsh])

 #make metadata file
 with open(metafn,'w') as wh:
  wh.write('gold_ap\t' + str(gap) + '\n')
  wh.write('ref_taxon\t' + str(reftaxon) + '\n')
  wh.write('bam_file\t' + str(bamfn) + '\n')
  wh.write('map_type\t' + str(maptype) + '\n')
  if jat_key_id : wh.write('jat_key\t' + str(jat_key_id) + '\n')

 #sys.exit(1)
    
