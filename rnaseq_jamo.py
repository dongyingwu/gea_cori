#! /usr/bin/env python
import requests, json, argparse
import os, re, sys
import db_connect
import startlog, rnaseq_pmos
from requests.packages.urllib3.exceptions import InsecureRequestWarning

def run(logging, check_keysfn, skip_keysfn):
 reqkeys = []
 if check_keysfn:
  with open(check_keysfn) as fh:
   for line in fh:
    jkey = line.rstrip('\n')
    reqkeys.append(jkey)

 skipkeys = []
 if skip_keysfn:
  with open(skip_keysfn) as fh:
   for line in fh:
    jkey = line.rstrip('\n')
    if re.search('^#',jkey) : continue
    skipkeys.append(jkey)

 jamo_releases = query_jat(reqkeys,skipkeys,logging)
 logging.info(str(len(jamo_releases.keys())) + ' releases to process')
 return(jamo_releases)


def query_jat(reqkeys,skipkeys,logging):
 to_process = {}
 base = "https://sdm2.jgi-psf.org/"
 url = base + "/api/analysis/query"
 requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

 #old template  ### uncommented on 06/07/2019
 data = {'template':'metatranscriptome_alignment', 'options.release_to':'img', 'omics.expression_sub_id':None}
 headers = {'Content-type': 'application/json'}
 response = requests.post(url, data=json.dumps(data), headers=headers)
 for release in response.json():
  process_release(reqkeys,skipkeys,release,to_process,logging)
 ### end uncommented on 06/07/2019

 #new template
 ad_data = {'template':'metatranscriptome_assembly_and_alignment','options.release_to':'img', 'omics.expression_sub_id':None, 'omics.expression_img_submission_id':None}
 ad_headers = {'Content-type': 'application/json'}
 ad_response = requests.post(url, data=json.dumps(ad_data), headers=ad_headers)
 for ad_release in ad_response.json():
  process_release(reqkeys,skipkeys,ad_release,to_process,logging)

 #new euk template
 ek_data = {'template':'eukaryotic_metatranscriptome_assembly_and_alignment','options.release_to':'img', 'omics.expression_sub_id':None, 'omics.expression_img_submission_id':None}
 ek_headers = {'Content-type': 'application/json'}
 ek_response = requests.post(url, data=json.dumps(ek_data), headers=ek_headers)
 for ek_release in ek_response.json():
  process_release(reqkeys,skipkeys,ek_release,to_process,logging)

 return(to_process)

def process_release(reqkeys,skipkeys,release,to_process,logging):
 jat_key = ref_taxonid = bam_floc = map_type = failure = None
# its_ap = its_sp = its_source_ap = gold_ap = gold_sp = None
 its_ap = its_sp = source_its_ap = gold_ap = gold_sp = None    ##modified on 06/07/2019
 #print release['key']

 if "img" not in release['options']['release_to']: return
 if len(reqkeys)>1 : 
  if release['key'] not in reqkeys: return
 if len(skipkeys)>1:
  if release['key'] in skipkeys: return

 year,day,monthtime = release['created'].split('-')
 jat_key = release['key']
 if int(year) < 2017 : return
 if jat_key == "AUTO-43629" : return
 
 print jat_key

 #get ITS AP for 'RNA EXPRESSION TASK'
 if release['template'] == 'metatranscriptome_assembly_and_alignment' or release['template'] == 'eukaryotic_metatranscriptome_assembly_and_alignment':
  source_its_ap = release['metadata']['analysis_project_id'][0]
  #its_sp = release['metadata']['sequencing_project']['sequencing_project_id']
  #its_ap = rnaseq_pmos.at_info(its_sp)
  #chagned to allow for combined assemblies
  its_ap = rnaseq_pmos.ap_info(source_its_ap)
 else:
  its_ap = release['metadata']['analysis_project_id'][0]
  if "source_analysis_project_id" in release['metadata']: 
   source_its_ap = release['metadata']['source_analysis_project_id'][0]

 #get GOLD AP
 if its_ap:
  ap_params = "/analysis_project/cached/" + str(its_ap)
  gold_ap = db_connect.gold_query_metadata(ap_params,'goldId')
 else:
  failure = "Cannot find analysis project with analysis task for RNAexpression with required status."
 
 #get reference taxon id using source AP info
 if source_its_ap:
  s_ap_params = "/analysis_project/cached/" + str(source_its_ap)
  ref_taxonid = db_connect.gold_query_metadata(s_ap_params,'imgTaxonOid')
  map_type = 'self'
 else:
  if 'img_dataset_id' in release['metadata']: 
   ref_taxonid =release['metadata']['img_dataset_id']
  map_type = 'other'

 #get file paths from release
 files_url = 'https://sdm2.jgi-psf.org/' + 'api/analysis/analysis/' + str(release['key']) + '/files'
 files_response = requests.get(files_url)
 for efile in files_response.json():
  if efile['label'] == 'metatranscriptome_alignment':
   bam_floc = efile['file_path'] + '/' + efile['file_name']

 if not failure: failure = check_metadata(jat_key, gold_ap, ref_taxonid, bam_floc)
 #print str(jat_key) + "\t" + str(its_ap) + "\t" + str(release['metadata']['sequencing_project_id'][0])

 if failure != '':
  logging.info("Failed metadata check of " + str(jat_key) + " : " + failure)
  #print "Failed validation of " + str(jat_key) + " : " + failure
  pass
 else:
  logging.info("Successfull metadata check of " + str(jat_key))
  to_process[jat_key] = {}
  to_process[jat_key]['gold_ap'] = gold_ap
  to_process[jat_key]['ref_taxonid'] = ref_taxonid
  to_process[jat_key]['bam_floc'] = bam_floc
  to_process[jat_key]['map_type'] = map_type
  to_process[jat_key]['database'] = 'IMG_MER_RNASEQ'

def check_metadata(jat_key, gold_ap, ref_taxonid, bam_floc):
 fail = ''
 if not ref_taxonid: fail += 'Reference taxon id is not annotated.'
 if not gold_ap: fail += 'GOLD AP cannot be found.'
 return(fail) 

def write_jat(key,value,jat_key_id):
 put_url = "https://sdm2.jgi-psf.org/api/analysis/metadata/" + jat_key_id
 token = 'KH40HYJFGA46YFW78VJ41I8WU8RV3D4I'
 pdata = {'metadata': {key:value}}
 pheader = {'Content-type': 'application/json','Authorization':'Application '+token}
 response = requests.put(put_url, data=json.dumps(pdata), headers=pheader)
 print put_url
 print pdata
 print response.status_code

if __name__ == '__main__':
 parser = argparse.ArgumentParser(description="JAMO pickup of metatranscriptome_alignment")
 parser.add_argument("--c", help = "file with jat keys to process (Optional)")
 parser.add_argument("--s", help = "file with jat keys to skip (Optional)")
 args = parser.parse_args()
 logging = startlog.run('analysis')
 run(logging,args.c,args.s)
