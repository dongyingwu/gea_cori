#! /usr/bin/env python
import sys,os,argparse, requests
import time, datetime, subprocess, hashlib
import startlog, rnaseq_pmos, rnaseq_jamo
import db_connect

def sub_metadata(meta):
 contact_id = 101782
 data = {}
 data['analysis_project_id'] = "'" + meta['gold_ap'] + "'"
 data['database'] = "'IMG_MER_RNASEQ'"
 data['contact'] = contact_id
 data['is_img_public'] = "'No'"
 data['submission_date'] = "SYSDATE"
 data['img_dev_flag'] = "'No'"
 data['add_to_portal'] = "'Yes'"
 data['mod_date'] = "SYSDATE"
 data['modified_by'] = contact_id
 data['bam_file'] = "'" + meta['bam_file'] + "'"
 data['jat_key_id'] = "'" + meta['jat_key'] + "'"
 if meta['map_type']  == "self" : data['set_embargo_date'] = "'No'"
 return(data)

def img_submit(sub_values, meta , gea_path, gea_done, logging):
 prod_dir = '/global/projectb/sandbox/omics/rnaseq/Metatranscriptomes/'
 ap_params = "/analysis_project/cached/" + str(meta['gold_ap'])
 its_ap = db_connect.gold_query_metadata(ap_params,'itsAnalysisProjectId')

 #get latest submission id
 qsql = 'select max(submission_id) from submission'
 max_sub_id = db_connect.imgsub_query(qsql)
 sub_id = max_sub_id[0][0] + 1
  
 #create submission entry in submission databas
 keys = values = ''
 for key in sub_values:
  if keys == '' : keys += str(key)
  else: keys += ',' + str(key)
  if values == '': values += str(sub_values[key])
  else: values += ',' + str(sub_values[key])

 isql = 'insert into submission (submission_id,' + keys + ') values (' + str(sub_id) + ',' + values + ')'
 logging.info(isql)
 db_connect.imgsub_write(isql)

 #rename and move file to sandbox
 new_gea = prod_dir + str(sub_id) + '.rnaseq_gea.txt'
 logging.info('Copying dev file:' + gea_path + ' to production file:' + new_gea)
 subprocess.check_call(['cp', gea_path, new_gea])
 
 #create entries in submission_data_files
 g_sql = 'insert into submission_data_files (submission_id,file_type,file_name,md5sum) values (' + str(sub_id) + ",'rnaseq_expression','" + new_gea + "','" + md5(new_gea) + "')"
 logging.info(g_sql)
 db_connect.imgsub_write(g_sql)

 #sucessful completion , final update of status to 40 so files can be picked up for loading into IMG
 usql = "update submission set status=40 where submission_id = " + str(sub_id) + " and database='IMG_MER_RNASEQ'"
 logging.info(usql)
 db_connect.imgsub_write(usql)

 #add expression sub id to JAT
 logging.info('Write to JAT:key=expression_img_submission_id,value=' + str(sub_id))
 rnaseq_jamo.write_jat('expression_img_submission_id', sub_id, meta['jat_key'])
 logging.info('Write to JAT:key=expression_analysis_project_id,value=' + str(its_ap))
 rnaseq_jamo.write_jat('expression_analysis_project_id', its_ap, meta['jat_key'])
 
 #mark scratch dir as done
 subprocess.check_call(['touch',gea_done])

 #set ITS AT to "In progress"
 ap_params = "/analysis_project/cached/" + str(meta['gold_ap'])
 its_ap = db_connect.gold_query_metadata(ap_params,'itsAnalysisProjectId')
 logging.info('Updating RNA Expression AT for its AP:'+ str(its_ap))
 rnaseq_pmos.update_at_status(its_ap)

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def validate_gea(fn):
 num_mapped_reads = 0
 non_zero_op = ''
 with open(fn) as fh:
  for line in fh:
   if line.startswith('img'): non_zero_op += line
   else:
    values = line.rstrip().split('\t')
    reads_cnt = values[7]
    if reads_cnt > 0:
     num_mapped_reads += int(reads_cnt)
     non_zero_op += line

 if num_mapped_reads >= 5000:
  #replace with non zero file
  new_fn = fn + '.txt'
  with open(new_fn,'w') as wh:
   wh.write(non_zero_op)
  return(new_fn)
 else:
  return('fail')

def run():
 logging = startlog.run('submission')
 oloc = os.getcwd()
 for dirname in os.listdir(oloc):
  dirpath = oloc + "/" + dirname
  gea_out = dirpath + '/' + 'rnaseq_gea.tab'
  gea_metadata = dirpath + '/' + 'metadata.tab'
  gea_done = dirpath + '/' + 'submit.complete'

  #logging.info('Dir: ' + dirpath)
  if os.path.isfile(gea_out) and os.path.isfile(gea_metadata) and not os.path.isfile(gea_done):
   validation = validate_gea(gea_out)
   if validation == 'fail': 
    logging.info('FAIL validation for ' + dirname + '\n')
   else:
    logging.info('Validated file with non zero values: ' + validation)
    metadata = {}
    with open(gea_metadata) as rh:
     for line in rh:
      key,value = line.rstrip().split('\t')
      metadata[key] = value

    sub_values = sub_metadata(metadata)
    img_submit(sub_values,metadata,validation,gea_done,logging)
  elif not os.path.isfile(gea_done):   
   logging.info('Incomplete analysis in ' + dirname + '\n')

   #sys.exit(1)

if __name__ == '__main__':
 parser = argparse.ArgumentParser(description="submission to database of completed metatranscriptome analysis")
 args = parser.parse_args()
 run()
